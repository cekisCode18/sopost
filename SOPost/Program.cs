﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPost
{
    class Program
    {
        static void Main(string[] args)
        {
            var soPost = new Post()
            {
                Title = "My First Post",
                Description = "I am new to StackOverflow and this is my first post. I have finally made it to the big time!"

            };

            bool continueLoop = true;

            do
            {
                Console.Clear();
                Console.WriteLine("StackOverflow Post #1:");
                Console.WriteLine($"Title: {soPost.Title}");
                Console.WriteLine($"Description: {soPost.Description}");
                Console.WriteLine($"Creation Date: {soPost.CreationDate}");
                Console.WriteLine();
                Console.WriteLine($"UpVotes: {soPost.UpVotes}");
                Console.WriteLine($"DownVotes: {soPost.DownVotes}");
                Console.WriteLine();
                Console.WriteLine("Press u to UpVote this post, d to DownVote this post, q to quit");


                char input = Console.ReadKey().KeyChar;

                switch (input)
                {
                    case 'u':
                        soPost.AddUpVote();
                        break;
                    case 'd':
                        soPost.AddDownVote();
                        break;
                    case 'q':
                        continueLoop = false;
                        break;
                }

            } while (continueLoop);

        }
    }
}
