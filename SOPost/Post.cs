﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPost
{
    public class Post
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; private set; }
        public int UpVotes { get; private set; }
        public int DownVotes { get; private set; }

        public Post()
        {
            this.CreationDate = DateTime.Now;
        }

        public void AddUpVote()
        {
            UpVotes++;
        }

        public void AddDownVote()
        {
            DownVotes++;
        }
    }
}
